package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TwoForOnePromotionShould {

    private TwoForOnePromotion twoForOnePromotion;
    private Tour operaHouse;
    private Tour sydneyBridgeClimb;
    private Tour sydneySkyTower;


    @BeforeEach
    void setUp() {
        operaHouse = new Tour("OH", "Opera house tour", Money.from(300.00));
        sydneyBridgeClimb = new Tour("BC", "Sydney Bridge Climb", Money.from(110.00));
        sydneySkyTower = new Tour("SK", "Sydney Sky Tower", Money.from(30.00));

        twoForOnePromotion = new TwoForOnePromotion(operaHouse, sydneySkyTower);
    }

    @Test
    void notCalculateDiscountIfNotDiscountedTourBooked() {
        PromotionDetails promotionDetails = twoForOnePromotion.calculatePromotion(List.of(sydneyBridgeClimb));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
        assertThat(promotionDetails.getTours().size()).isEqualTo(1);
    }

    @Test
    void calculateDiscountIfDiscountedTourBooked() {
        PromotionDetails promotionDetails = twoForOnePromotion.calculatePromotion(List.of(operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(sydneySkyTower.getPrice());
        assertThat(promotionDetails.getTours().size()).isEqualTo(2);
        assertThat(promotionDetails.getTours()).containsExactly(operaHouse, sydneySkyTower);
    }

    @Test
    void calculateDiscountIfDoubleDiscountedTourBooked() {
        PromotionDetails promotionDetails = twoForOnePromotion.calculatePromotion(List.of(operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(60.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(4);
        assertThat(promotionDetails.getTours()).containsExactly(operaHouse, operaHouse, sydneySkyTower, sydneySkyTower);
    }
}