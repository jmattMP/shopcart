package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class GroupTourCheaperPromotionShould {

    private GroupTourCheaperPromotion groupTourCheaperPromotion;
    private Tour operaHouse;
    private Tour sydneyBridgeClimb;
    private Tour sydneySkyTower;

    @BeforeEach
    void setUp() {
        operaHouse = new Tour("OH", "Opera house tour", Money.from(300.00));
        sydneyBridgeClimb = new Tour("BC", "Sydney Bridge Climb", Money.from(110.00));
        sydneySkyTower = new Tour("SK", "Sydney Sky Tower", Money.from(30.00));

        groupTourCheaperPromotion = new GroupTourCheaperPromotion(sydneyBridgeClimb, 4, Money.from(20.00));
    }

    @Test
    void notCalculatePromotionIfLessTheSameToursThenSpecified() {
        PromotionDetails promotionDetails = groupTourCheaperPromotion.calculatePromotion(List.of(sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb));
        assertThat(promotionDetails.getTours().size()).isEqualTo(3);
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
    }

    @Test
    void notCalculatePromotionIfMoreThenSpecifiedButDifferentTours() {
        PromotionDetails promotionDetails = groupTourCheaperPromotion.calculatePromotion(List.of(operaHouse, sydneyBridgeClimb, sydneySkyTower, operaHouse, sydneyBridgeClimb, sydneySkyTower));
        assertThat(promotionDetails.getTours().size()).isEqualTo(6);
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
    }

    @Test
    void notCalculatePromotionIfEqualThenSpecified() {
        PromotionDetails promotionDetails = groupTourCheaperPromotion.calculatePromotion(List.of(sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb));
        assertThat(promotionDetails.getTours().size()).isEqualTo(4);
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
    }

    @Test
    void calculatePromotionIfMoreThenSpecified() {
        PromotionDetails promotionDetails = groupTourCheaperPromotion.calculatePromotion(List.of(sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb));
        assertThat(promotionDetails.getTours().size()).isEqualTo(5);
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(20.0));
    }
}