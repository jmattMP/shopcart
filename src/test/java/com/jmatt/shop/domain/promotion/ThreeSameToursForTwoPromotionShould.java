package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ThreeSameToursForTwoPromotionShould {

    private ThreeSameToursForTwoPromotion threeSameToursForTwoPromotion;
    private Tour operaHouse;
    private Tour sydneyBridgeClimb;
    private Tour sydneySkyTower;


    @BeforeEach
    void setUp() {
        operaHouse = new Tour("OH", "Opera house tour", Money.from(300.00));
        sydneyBridgeClimb = new Tour("BC", "Sydney Bridge Climb", Money.from(110.00));
        sydneySkyTower = new Tour("SK", "Sydney Sky Tower", Money.from(30.00));

        threeSameToursForTwoPromotion = new ThreeSameToursForTwoPromotion(operaHouse);
    }

    @Test
    void notCalculatePromotionWhenLessThenThree() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
        assertThat(promotionDetails.getTours().size()).isEqualTo(2);
    }

    @Test
    void notCalculateWhenThreeDifferentToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, sydneyBridgeClimb, sydneySkyTower));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
        assertThat(promotionDetails.getTours().size()).isEqualTo(3);
    }

    @Test
    void notCalculateWhenThreeTheSameButNotDiscountedToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.fromZero());
        assertThat(promotionDetails.getTours().size()).isEqualTo(3);
    }

    @Test
    void calculateWhenThreeTheSameDiscountedToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(300.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(3);
    }

    @Test
    void calculateWhenFourTheSameDiscountedToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse, operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(300.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(4);
    }

    @Test
    void calculateWhenFourToursBookedButOnlyThreeDiscounted() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse, operaHouse, sydneyBridgeClimb));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(300.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(4);
    }

    @Test
    void calculateDoubleWhenSixTheSameToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse, operaHouse, operaHouse, operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(600.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(6);
    }

    @Test
    void calculateDoubleWhenSevenTheSameToursBooked() {
        PromotionDetails promotionDetails = threeSameToursForTwoPromotion.calculatePromotion(List.of(operaHouse, operaHouse, operaHouse, operaHouse, operaHouse, operaHouse, operaHouse));
        assertThat(promotionDetails.getDiscount()).isEqualByComparingTo(Money.from(600.00));
        assertThat(promotionDetails.getTours().size()).isEqualTo(7);
    }

}