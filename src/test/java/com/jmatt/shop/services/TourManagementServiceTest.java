package com.jmatt.shop.services;

import com.jmatt.shop.services.ports.driven.TourManagementRepository;
import com.jmatt.shop.services.ports.driving.TourManagementUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class TourManagementServiceTest {
    @Mock
    private TourManagementRepository tourManagementRepository;

    private TourManagementUseCase tourManagementUseCase;

    @BeforeEach
    void setUp() {
        // Given
    }

    @Test
    void addTourToOffer(){
        // Given

        // When

        // Then
    }

    @Test
    void tryCreateWrongFormatTour(){
        // Given

        // When

        // Then
    }

    @Test
    void removeTourFromOffer(){
        // Given

        // When

        // Then
    }
}