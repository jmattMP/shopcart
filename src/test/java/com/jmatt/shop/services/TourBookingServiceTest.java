package com.jmatt.shop.services;

import com.jmatt.shop.domain.cart.ShoppingCart;
import com.jmatt.shop.domain.cart.ShoppingCartNotFoundException;
import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.promotion.*;
import com.jmatt.shop.domain.tour.Tour;
import com.jmatt.shop.services.ports.driven.ShoppingCartRepository;
import com.jmatt.shop.services.ports.driven.TourRepository;
import com.jmatt.shop.services.ports.driving.TourBookingUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class TourBookingServiceTest {

    @Mock
    private TourRepository tourRepository;
    @Mock
    private ShoppingCartRepository shoppingCartRepository;

    private TourBookingUseCase tourBookingUseCase;
    private Collection<Tour> availableTours;
    private Tour operaHouse;
    private Tour sydneyBridgeClimb;
    private Tour sydneySkyTower;
    private String cartId;
    private ShoppingCart testedCart;


    @BeforeEach
    void setUp() {
        // Given
        operaHouse = new Tour("OH", "Opera house tour", Money.from(300.00));
        sydneyBridgeClimb = new Tour("BC", "Sydney Bridge Climb", Money.from(110.00));
        sydneySkyTower = new Tour("SK", "Sydney Sky Tower", Money.from(30.00));

        GroupTourCheaperPromotion groupTourCheaperPromotion = new GroupTourCheaperPromotion(sydneyBridgeClimb, 4, Money.from(20.00));
        ThreeSameToursForTwoPromotion threeSameToursForTwoPromotion = new ThreeSameToursForTwoPromotion(operaHouse);
        TwoForOnePromotion twoForOnePromotion = new TwoForOnePromotion(operaHouse, sydneySkyTower);

        availableTours = List.of(operaHouse, sydneyBridgeClimb, sydneySkyTower);

        List<PromotionPolicy> availablePromotions = List.of(groupTourCheaperPromotion, threeSameToursForTwoPromotion, twoForOnePromotion);

        PromotionProvider promotionProvider = new DefaultPromotionProvider(availablePromotions);

        tourBookingUseCase = new TourBookingService(tourRepository, shoppingCartRepository, promotionProvider);

        testedCart = tourBookingUseCase.createCart();

        cartId = testedCart.getShoppingCartId();
    }

    @Test
    void bookToursAndGetSummaryWithCalculatedPromotion() {
        // Given
        given(tourRepository.getAvailableTours()).willReturn(availableTours);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));
        given(tourRepository.getTour(eq("SK"))).willReturn(Optional.of(sydneySkyTower));
        given(tourRepository.getTour(eq("BC"))).willReturn(Optional.of(sydneyBridgeClimb));
        given(shoppingCartRepository.getShoppingCart(eq(cartId))).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);

        Collection<Tour> returnedTours = tourBookingUseCase.getAvailableTours();
        assertThat(returnedTours).containsAll(availableTours);

        tourBookingUseCase.bookTour(operaHouse.getTourId(), cartId);
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        tourBookingUseCase.removeTourFromCart(sydneyBridgeClimb.getTourId(), cartId);
        assertThat(testedCart.getAllAddedTours()).hasSize(1);
        assertThat(testedCart.getAllAddedTours()).containsOnly(operaHouse);

        tourBookingUseCase.removeTourFromCart(operaHouse.getTourId(), cartId);

        //promotion two for one
        tourBookingUseCase.bookTour(operaHouse.getTourId(), cartId);
        ShoppingCart promotedCart = tourBookingUseCase.getBookedToursSummary(cartId);
        assertThat(promotedCart.getAllAddedTours()).hasSize(2);
        assertThat(promotedCart.getAllAddedTours()).containsOnly(operaHouse, sydneySkyTower);
        assertThat(promotedCart.getTotalPrice()).isEqualByComparingTo(Money.from(300.00));

        tourBookingUseCase.removeTourFromCart(operaHouse.getTourId(), cartId);
        tourBookingUseCase.removeTourFromCart(sydneySkyTower.getTourId(), cartId);

        //promotion Three For Two The Same
        tourBookingUseCase.bookTour(operaHouse.getTourId(), cartId);
        tourBookingUseCase.bookTour(operaHouse.getTourId(), cartId);
        tourBookingUseCase.bookTour(operaHouse.getTourId(), cartId);
        promotedCart = tourBookingUseCase.getBookedToursSummary(cartId);
        assertThat(promotedCart.getAllAddedTours()).hasSize(3);
        assertThat(promotedCart.getAllAddedTours()).containsOnly(operaHouse, operaHouse, operaHouse);
        assertThat(promotedCart.getTotalPrice()).isEqualByComparingTo(Money.from(600.00));

        tourBookingUseCase.removeTourFromCart(operaHouse.getTourId(), cartId);
        tourBookingUseCase.removeTourFromCart(operaHouse.getTourId(), cartId);
        tourBookingUseCase.removeTourFromCart(operaHouse.getTourId(), cartId);

        //promotion group tours cheaper
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        tourBookingUseCase.bookTour(sydneyBridgeClimb.getTourId(), cartId);
        promotedCart = tourBookingUseCase.getBookedToursSummary(cartId);
        assertThat(promotedCart.getAllAddedTours()).hasSize(5);
        assertThat(promotedCart.getAllAddedTours()).containsOnly(sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb, sydneyBridgeClimb);
        assertThat(promotedCart.getTotalPrice()).isEqualByComparingTo(Money.from(530.00));
    }

    @Test
    void returnAvailableTours() {
        // Given
        given(tourRepository.getAvailableTours()).willReturn(availableTours);

        // When
        Collection<Tour> returnedTours = tourBookingUseCase.getAvailableTours();

        // Then
        assertThat(returnedTours).containsAll(availableTours);
    }

    @Test
    void returnSpecifiedTour() {
        // Given
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));
        given(tourRepository.getTour(eq("SK"))).willReturn(Optional.of(sydneySkyTower));
        given(tourRepository.getTour(eq("BC"))).willReturn(Optional.of(sydneyBridgeClimb));

        // When
        Tour oh = tourBookingUseCase.getTour("OH").get();
        Tour sk = tourBookingUseCase.getTour("SK").get();
        Tour bc = tourBookingUseCase.getTour("BC").get();
        Collection<Tour> returnedTours = List.of(oh, sk, bc);

        // Then
        assertThat(returnedTours).containsAll(availableTours);
    }

    @Test
    void addTourToCart() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));
        given(tourRepository.getTour(eq("SK"))).willReturn(Optional.of(sydneySkyTower));
        given(tourRepository.getTour(eq("BC"))).willReturn(Optional.of(sydneyBridgeClimb));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("SK", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("BC", cartId);

        // Then
        assertThat(returnedCart.getAllAddedTours()).containsAll(availableTours);
    }

    @Test
    void tryAddTourToNotExistingCart() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.empty());

        // When
        Throwable thrown = catchThrowable(() -> tourBookingUseCase.bookTour("OH", cartId));

        // Then
        assertThat(thrown).isInstanceOf(ShoppingCartNotFoundException.class)
                .hasMessage("Shopping Cart %s not found", cartId);
    }

    @Test
    void removeTourFromCart() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));
        given(tourRepository.getTour(eq("SK"))).willReturn(Optional.of(sydneySkyTower));
        given(tourRepository.getTour(eq("BC"))).willReturn(Optional.of(sydneyBridgeClimb));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("SK", cartId);
        tourBookingUseCase.bookTour("BC", cartId);
        ShoppingCart returnedShoppingCart = tourBookingUseCase.removeTourFromCart("OH", cartId);

        // Then
        assertThat(returnedShoppingCart.getAllAddedTours()).containsAll(List.of(sydneyBridgeClimb, sydneySkyTower));
    }

    @Test
    void removeTourFromNotExistingCart() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.empty());

        // When
        Throwable thrown = catchThrowable(() -> tourBookingUseCase.removeTourFromCart("OH", cartId));

        // Then
        assertThat(thrown).isInstanceOf(ShoppingCartNotFoundException.class)
                .hasMessage("Shopping Cart %s not found", cartId);
    }

    @Test
    void clearCart() {
        // Given
        given(shoppingCartRepository.removeShoppingCart(cartId)).willReturn(true);

        // When
        boolean isRemoved = tourBookingUseCase.removeAllFromCart(cartId);

        // Then
        assertThat(isRemoved).isTrue();
    }

    @Test
    void testIfThrowExceptionWhenCheckingSummaryOnCartWhichNotExist() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.empty());

        // When
        Throwable thrown = catchThrowable(() -> tourBookingUseCase.getBookedToursSummary(cartId));

        // Then
        assertThat(thrown).isInstanceOf(ShoppingCartNotFoundException.class)
                .hasMessage("Shopping Cart %s not found", cartId);
    }

    @Test
    void testIfBuyingThreeOperaHouseTicketYouWillPayForTwo() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("OH", cartId);
        Money totalValue = returnedCart.getTotalPrice();
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(totalValue.getAmount()).isEqualByComparingTo(Money.from(900.00).getAmount());
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(600.0).getAmount());
    }

    @Test
    void testIfBuyingFiveOperaHouseTicketYouWillPayForFour() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("OH", cartId);
        Money totalValue = returnedCart.getTotalPrice();
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(totalValue.getAmount()).isEqualByComparingTo(Money.from(1500.00).getAmount());
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(1200.0).getAmount());
    }

    @Test
    void testIfBuyingSixOperaHouseTicketYouWillPayForFour() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("OH", cartId);
        Money totalValue = returnedCart.getTotalPrice();
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(totalValue.getAmount()).isEqualByComparingTo(Money.from(1800.0).getAmount());
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(1200.0).getAmount());
    }

    @Test
    void testIfBuyingOneOperaHouseTicketYouWillGetSkyTowerForFree() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(300.00).getAmount());
        assertThat(summaryShoppingCart.getAllAddedTours()).hasSize(2);
        assertThat(summaryShoppingCart.getAllAddedTours()).containsAll(List.of(operaHouse, sydneySkyTower));
    }

    @Test
    void testIfBuyingTwoOperaHouseTicketYouWillGetTwoSkyTowerForFree() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(600.00).getAmount());
        assertThat(returnedCart.getAllAddedTours()).hasSize(4);
        assertThat(returnedCart.getAllAddedTours()).containsAll(List.of(operaHouse, sydneySkyTower, operaHouse, sydneySkyTower));
    }

    @Test
    void testIfBuyingThreeOperaHouseTicketYouWillGetOneOperaHouseForFreeInsteadThreeSkyTowerForFree() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("OH"))).willReturn(Optional.of(operaHouse));

        // When
        tourBookingUseCase.bookTour("OH", cartId);
        tourBookingUseCase.bookTour("OH", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("OH", cartId);

        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(600.00).getAmount());
        assertThat(returnedCart.getAllAddedTours()).hasSize(3);
        assertThat(returnedCart.getAllAddedTours()).containsAll(List.of(operaHouse, operaHouse, operaHouse));
    }

    @Test
    void testIfBuyingMoreThenFourSydneyBridgeClimbToursYouWillPay20DollarsLess() {
        // Given
        given(shoppingCartRepository.getShoppingCart(cartId)).willReturn(Optional.of(testedCart));
        given(shoppingCartRepository.saveShoppingCart(eq(testedCart))).willReturn(testedCart);
        given(tourRepository.getTour(eq("BC"))).willReturn(Optional.of(sydneyBridgeClimb));

        // When
        tourBookingUseCase.bookTour("BC", cartId);
        tourBookingUseCase.bookTour("BC", cartId);
        tourBookingUseCase.bookTour("BC", cartId);
        tourBookingUseCase.bookTour("BC", cartId);
        ShoppingCart returnedCart = tourBookingUseCase.bookTour("BC", cartId);

        Money totalValue = returnedCart.getTotalPrice();
        ShoppingCart summaryShoppingCart = tourBookingUseCase.getBookedToursSummary(cartId);
        Money summaryTotalPrice = summaryShoppingCart.getTotalPrice();

        // Then
        assertThat(totalValue.getAmount()).isEqualByComparingTo(Money.from(550.00).getAmount());
        assertThat(summaryTotalPrice.getAmount()).isEqualByComparingTo(Money.from(530.00).getAmount());
        assertThat(returnedCart.getAllAddedTours()).hasSize(5);
        assertThat(returnedCart.getAllAddedTours()).containsAll(List.of(sydneyBridgeClimb));
    }
}