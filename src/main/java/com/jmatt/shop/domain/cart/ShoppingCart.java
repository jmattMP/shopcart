package com.jmatt.shop.domain.cart;

import com.jmatt.shop.domain.commons.IdentityGenerator;
import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShoppingCart {
    private static final Logger logger = Logger.getLogger(ShoppingCart.class.getName());
    private String shoppingCartId;
    private Collection<Tour> tours;
    private Money totalPrice;

    public boolean addTour(Tour tour) {
        logger.log(Level.INFO, "Adding Tour {0} to cart {1}", new Object[]{tour.getTourName(), this.shoppingCartId});
        totalPrice = totalPrice.add(tour.getPrice());
        return tours.add(tour);
    }

    public boolean removeTour(Tour tour) {
        logger.log(Level.INFO, "Removing Tour {0} from cart {1}", new Object[]{tour.getTourName(), this.shoppingCartId});
        totalPrice = totalPrice.subtract(tour.getPrice());
        return tours.remove(tour);
    }

    public Collection<Tour> getAllAddedTours() {
        return tours;
    }

    public static ShoppingCart createShoppingCart() {
        return new ShoppingCart();
    }

    public static ShoppingCart createShoppingCartFrom(ShoppingCart shoppingCart, Collection<Tour> tours, Money totalPrice) {
        return new ShoppingCart(shoppingCart, tours, totalPrice);
    }

    private ShoppingCart() {
        this.shoppingCartId = IdentityGenerator.generateIdentityBasedOnDate();
        this.totalPrice = new Money(BigDecimal.valueOf(0));
        this.tours = new ArrayList<>();
    }

    private ShoppingCart(ShoppingCart shoppingCart, Collection<Tour> tours, Money totalPrice) {
        this.shoppingCartId = shoppingCart.getShoppingCartId();
        this.tours = new ArrayList<>(tours);
        this.totalPrice = totalPrice;
    }

    public Money getTotalPrice() {
        return totalPrice;
    }

    public String getShoppingCartId() {
        return shoppingCartId;
    }

    public void swapTours(Collection<Tour> tours) {
        logger.log(Level.INFO, "Swapping Tours in cart {0}", this.shoppingCartId);
        this.tours = new ArrayList<>(tours);
        this.totalPrice = tours.stream().map(Tour::getPrice).reduce(Money.fromZero(), Money::add);
    }
}

