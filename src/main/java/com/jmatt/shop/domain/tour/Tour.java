package com.jmatt.shop.domain.tour;

import com.jmatt.shop.domain.commons.Money;

import java.security.InvalidParameterException;
import java.util.Objects;

public class Tour {
    private final String tourId;
    private final String tourName;
    private final Money price;

    public Tour(String tourId, String tourName, Money price) {
        Objects.requireNonNull(tourId);
        Objects.requireNonNull(tourName);
        Objects.requireNonNull(price);

        if (price.getAmount().doubleValue() < 0.00) {
            throw new InvalidParameterException("Price cannot be negative");
        }
        if (tourName.isEmpty() || tourName.length() < 10) {
            throw new InvalidParameterException("Tour name cannot be empty or less then 10 characters");
        }
        if ((tourId.length() != 2)) {
            throw new InvalidParameterException("Tour id should has 2 chars");
        }

        this.tourId = tourId;
        this.tourName = tourName;
        this.price = price;
    }

    public String getTourId() {
        return tourId;
    }

    public String getTourName() {
        return tourName;
    }

    public Money getPrice() {
        return price;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        Tour other = (Tour) o;
        return tourId.equals(other.getTourId());
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + tourId.hashCode();
        return result;
    }
}
