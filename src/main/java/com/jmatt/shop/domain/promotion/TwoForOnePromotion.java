package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TwoForOnePromotion implements PromotionPolicy {
    private static final Logger logger = Logger.getLogger(TwoForOnePromotion.class.getName());
    private final String promotionName;
    private final Tour promotedTour;
    private final Tour freeTour;

    /**
     * @param promotedTour determines tour which is taken into account in order to gain promotion
     * @param freeTour     determines tour which will be for free if @promotedTour will be booked
     */
    public TwoForOnePromotion(Tour promotedTour, Tour freeTour) {
        this.promotedTour = promotedTour;
        this.freeTour = freeTour;
        this.promotionName = "Two for one promotion";
    }

    @Override
    public PromotionDetails calculatePromotion(Collection<Tour> tours) {
        logger.log(Level.INFO, "Calculating promotion {0}", promotionName);
        Collection<Tour> discountedTours = tours.stream().filter(tour -> tour.equals(promotedTour)).collect(Collectors.toList());

        if (discountedTours.isEmpty()) {
            logger.log(Level.INFO, "Promotion {0} not applied because not discounted tours in cart", promotionName);
            return new PromotionDetails(promotionName, Money.fromZero(), tours);
        }

        Collection<Tour> freeTours = new ArrayList<>();
        Money discount = Money.from(freeTour
                .getPrice()
                .getAmount()
                .multiply(BigDecimal.valueOf(discountedTours.size())));

        discountedTours.forEach(tour -> freeTours.add(freeTour));

        Collection<Tour> allTours = new ArrayList<>(tours);
        allTours.addAll(freeTours);
        logger.log(Level.INFO, "Promotion details - promotion name {0}, promotion discount {1} ", new Object[]{promotionName, discount});
        return new PromotionDetails(promotionName, discount, allTours);
    }

}
