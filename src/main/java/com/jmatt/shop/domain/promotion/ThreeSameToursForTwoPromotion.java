package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ThreeSameToursForTwoPromotion implements PromotionPolicy {
    private static final Logger logger = Logger.getLogger(ThreeSameToursForTwoPromotion.class.getName());
    private final String promotionName;
    private final Tour promotedTour;

    /**
     * @param promotedTour determines tour which is taken into account in order to gain promotion
     */
    public ThreeSameToursForTwoPromotion(Tour promotedTour) {
        this.promotedTour = promotedTour;
        this.promotionName = "Three for two promotion";
    }

    @Override
    public PromotionDetails calculatePromotion(Collection<Tour> tours) {
        logger.log(Level.INFO, "Calculating promotion {0}", promotionName);
        Collection<Tour> discountedTours = tours.stream().filter(tour -> tour.equals(promotedTour)).collect(Collectors.toList());
        if (discountedTours.size() < 3) {
            logger.log(Level.INFO, "Promotion {0} not applied because less then needed", promotionName);
            return new PromotionDetails(promotionName, Money.fromZero(), tours);
        }
        BigDecimal discount = promotedTour.getPrice().getAmount().multiply(BigDecimal.valueOf((discountedTours.size() / 3L)));

        logger.log(Level.INFO, "Promotion details - promotion name {0}, promotion discount {1} ", new Object[]{promotionName, discount});
        return new PromotionDetails(promotionName, Money.from(discount), tours);
    }
}
