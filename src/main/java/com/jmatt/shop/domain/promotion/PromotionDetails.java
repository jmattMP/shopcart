package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.util.ArrayList;
import java.util.Collection;

public class PromotionDetails {
    private final String promotionName;
    private final Collection<Tour> tours;
    private final Money discount;

    public PromotionDetails(String promotionName, Money discount, Collection<Tour> tours) {
        this.promotionName = promotionName;
        this.discount = discount;
        this.tours = new ArrayList<>(tours);
    }

    public String getPromotionName() {
        return promotionName;
    }

    public Money getDiscount() {
        return discount;
    }

    public Collection<Tour> getTours() {
        return new ArrayList<>(tours);
    }
}
