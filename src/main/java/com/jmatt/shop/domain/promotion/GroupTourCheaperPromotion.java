package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class GroupTourCheaperPromotion implements PromotionPolicy {
    private static final Logger logger = Logger.getLogger(GroupTourCheaperPromotion.class.getName());
    private final String promotionName;
    private final Tour discountedTour;
    private final int buyMoreThen;
    private final Money receivedDiscount;

    /**
     * @param discountedTour   determines tour which is taken into account in order to gain promotion
     * @param buyMoreThen      determines number of @param discountedTour which have to be booked in order to gain promotion
     * @param receivedDiscount determines amount the price will be dropped
     */
    public GroupTourCheaperPromotion(Tour discountedTour, int buyMoreThen, Money receivedDiscount) {
        this.discountedTour = discountedTour;
        this.buyMoreThen = buyMoreThen;
        this.receivedDiscount = receivedDiscount;
        this.promotionName = "Group tour promotion";
    }

    @Override
    public PromotionDetails calculatePromotion(Collection<Tour> tours) {
        logger.log(Level.INFO, "Calculating promotion {0}", promotionName);
        Collection<Tour> discountedTours = tours.stream().filter(tour -> tour.getTourId().equals(discountedTour.getTourId())).collect(Collectors.toList());
        if (discountedTours.size() <= buyMoreThen) {
            logger.log(Level.INFO, "Promotion {0} not applied because less then needed", promotionName);
            return new PromotionDetails(promotionName, Money.fromZero(), tours);
        }
        logger.log(Level.INFO, "Promotion details - promotion name {0}, promotion discount {1} ", new Object[]{promotionName, receivedDiscount});
        return new PromotionDetails(promotionName, receivedDiscount, tours);
    }
}
