package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.tour.Tour;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultPromotionProvider implements PromotionProvider {
    private static final Logger logger = Logger.getLogger(DefaultPromotionProvider.class.getName());
    private Collection<PromotionPolicy> promotionPolicies;

    public DefaultPromotionProvider(Collection<PromotionPolicy> promotionPolicies) {
        this.promotionPolicies = promotionPolicies;
    }

    @Override
    public PromotionDetails calculatePromotion(Collection<Tour> tours) {
        logger.info("Calculating promotions");
        Collection<PromotionDetails> calculatedPromotions = new ArrayList<>();
        promotionPolicies.forEach(promotionPolicy -> calculatedPromotions.add(promotionPolicy.calculatePromotion(tours)));

        if (calculatedPromotions.isEmpty()) {
            logger.info("None of promotion applied");
            return new PromotionDetails(
                    "none", Money.fromZero(), tours);
        }

        Optional<PromotionDetails> promotionDetailsOptional = calculatedPromotions.stream()
                .max(Comparator.comparing(PromotionDetails::getDiscount));

        PromotionDetails pD = promotionDetailsOptional.orElseGet(() -> calculatedPromotions.stream().max(Comparator.comparing(promotionDetails -> promotionDetails.getTours().size())).get());

        logger.log(Level.INFO, "Promotion calculated and choosen - promotion name {0}, promotion discount {1}", new Object[]{pD.getPromotionName(), pD.getDiscount()});
        return new PromotionDetails(pD.getPromotionName(), pD.getDiscount(), pD.getTours());
    }
}
