package com.jmatt.shop.domain.promotion;

import com.jmatt.shop.domain.tour.Tour;

import java.util.Collection;

public interface PromotionProvider {
    PromotionDetails calculatePromotion(Collection<Tour> tours);
}
