package com.jmatt.shop.domain.commons;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

/**
 * Now only support AUD as currency
 */
public class Money implements Comparable<Money> {
    private final BigDecimal amount;
    private final Currency currency;

    public Money(BigDecimal amount) {
        this.currency = Currency.getInstance("AUD");
        this.amount = Objects.requireNonNull(amount);
    }

    public static Money fromZero() {
        return new Money(BigDecimal.valueOf(0));
    }

    public static Money from(double amount) {
        return new Money(BigDecimal.valueOf(amount));
    }

    public static Money from(BigDecimal bigDecimal) {
        return new Money(bigDecimal);
    }

    public Money add(Money other) {
        return new Money(amount.add(other.amount));
    }

    public Money subtract(Money other) {
        return new Money(amount.subtract(other.amount));
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        Money other = (Money) o;
        return amount.doubleValue() == other.getAmount().doubleValue();
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + amount.hashCode();
        return result;
    }

    public int compareTo(Money other) {
        return amount.compareTo(other.getAmount());
    }
}
