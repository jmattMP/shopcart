package com.jmatt.shop.domain.commons;

import java.time.LocalDate;
import java.util.UUID;

public class IdentityGenerator {
    public static String generateIdentityBasedOnDate() {
        return LocalDate.now().toString() + "-" + UUID.randomUUID().toString();
    }

    private IdentityGenerator() {
        throw new IllegalStateException("Utility class");
    }
}