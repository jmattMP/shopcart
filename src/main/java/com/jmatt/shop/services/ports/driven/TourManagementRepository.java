package com.jmatt.shop.services.ports.driven;

import com.jmatt.shop.domain.tour.Tour;

public interface TourManagementRepository {
    boolean addTour(Tour tour);
    boolean removeTour(String tourId);
}
