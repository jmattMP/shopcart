package com.jmatt.shop.services.ports.driving;

import com.jmatt.shop.domain.tour.Tour;

/**
 * API for external components for administer tours option
 * Warning! NOT ACCESSIBLE IN THIS VERSION
 */
public interface TourManagementUseCase {
    /**
     * @param tour add tour specify with this parameter
     * @return true if tour is added and false if tour is not added
     */
    boolean addTourToOffer(Tour tour);

    /**
     * @param tourId remove tour specify with tourId
     * @return true if tour is removed and false if tour is not removed
     */
    boolean removeTourFromOffer(String tourId);
}
