package com.jmatt.shop.services.ports.driven;

import com.jmatt.shop.domain.cart.ShoppingCart;

import java.util.Optional;

public interface ShoppingCartRepository {
    boolean removeShoppingCart(String shoppingCartId);
    Optional<ShoppingCart> getShoppingCart(String shoppingCartId);
    ShoppingCart saveShoppingCart(ShoppingCart shoppingCart);
}
