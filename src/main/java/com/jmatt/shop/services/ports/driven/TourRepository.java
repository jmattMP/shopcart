package com.jmatt.shop.services.ports.driven;

import com.jmatt.shop.domain.tour.Tour;

import java.util.Collection;
import java.util.Optional;

public interface TourRepository {
    Collection<Tour> getAvailableTours();
    Optional<Tour> getTour(String tourId);
}
