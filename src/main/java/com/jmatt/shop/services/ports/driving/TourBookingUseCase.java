package com.jmatt.shop.services.ports.driving;

import com.jmatt.shop.domain.cart.ShoppingCart;
import com.jmatt.shop.domain.cart.ShoppingCartNotFoundException;
import com.jmatt.shop.domain.tour.Tour;
import com.jmatt.shop.domain.tour.TourNotFoundException;

import java.util.Collection;
import java.util.Optional;

/**
 * API for external components for creating shopping cart and booking tours
 */
public interface TourBookingUseCase {
    /**
     * @return all tours which are available in current timeline
     */
    Collection<Tour> getAvailableTours();

    /**
     * @param tourId alphabetic identity for specify tour
     * @return Optional Tour identified though @param tourId or Optional empty
     */
    Optional<Tour> getTour(String tourId);

    /**
     * Create new cart and return id of cart
     *
     * @return identity of already created cart
     */
    ShoppingCart createCart();

    /**
     * @param tourId         alphabetic identity for specify tour
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId with recently added tour to this cart
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not found
     * @throws TourNotFoundException         is throw when tour with @param tourId not found
     */
    ShoppingCart bookTour(String tourId, String shoppingCartId);

    /**
     * @param tourId         alphabetic identity for specify tour
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId with recently removed tour to this cart
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not found
     * @throws TourNotFoundException         is throw when tour with @param tourId not found
     */
    ShoppingCart removeTourFromCart(String tourId, String shoppingCartId);

    /**
     * @param shoppingCartId numeric identity for specify shopping cart
     * @return shopping cart specified with @param shoppingCartId with calculated promotion
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not found
     */
    ShoppingCart getBookedToursSummary(String shoppingCartId);

    /**
     * @param shoppingCartId shoppingCartId numeric identity for specify shopping cart
     * @return true if cart is cleared, false if cart not cleared
     * @throws ShoppingCartNotFoundException is throw when cart with @param shoppingCartId not found
     */
    boolean removeAllFromCart(String shoppingCartId);
}
