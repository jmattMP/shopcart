package com.jmatt.shop.services;

import com.jmatt.shop.domain.cart.ShoppingCart;
import com.jmatt.shop.domain.cart.ShoppingCartNotFoundException;
import com.jmatt.shop.domain.commons.Money;
import com.jmatt.shop.domain.promotion.PromotionDetails;
import com.jmatt.shop.domain.promotion.PromotionProvider;
import com.jmatt.shop.domain.tour.Tour;
import com.jmatt.shop.domain.tour.TourNotFoundException;
import com.jmatt.shop.services.ports.driven.ShoppingCartRepository;
import com.jmatt.shop.services.ports.driven.TourRepository;
import com.jmatt.shop.services.ports.driving.TourBookingUseCase;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TourBookingService implements TourBookingUseCase {
    private static final Logger logger = Logger.getLogger(TourBookingService.class.getName());
    private final TourRepository tourRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final PromotionProvider promotionProvider;


    public TourBookingService(TourRepository tourRepository, ShoppingCartRepository shoppingCartRepository, PromotionProvider promotionProvider) {
        this.tourRepository = Objects.requireNonNull(tourRepository);
        this.shoppingCartRepository = Objects.requireNonNull(shoppingCartRepository);
        this.promotionProvider = Objects.requireNonNull(promotionProvider);
    }

    @Override
    public Collection<Tour> getAvailableTours() {
        logger.info("Returning available tours");
        return tourRepository.getAvailableTours();
    }

    @Override
    public Optional<Tour> getTour(String tourId) {
        logger.log(Level.INFO, "Returning tour with id {0}", tourId);
        return tourRepository.getTour(tourId);
    }

    @Override
    public ShoppingCart createCart() {
        logger.info("Creating new cart");
        return ShoppingCart.createShoppingCart();
    }

    @Override
    public ShoppingCart bookTour(String tourId, String shoppingCartId) {
        logger.log(Level.INFO, "Booking tour with id {0} for cart with id {1}", new Object[]{tourId, shoppingCartId});
        ShoppingCart shoppingCart = checkIfCartExist(shoppingCartId);
        shoppingCart.addTour(checkIfTourExist(tourId));
        shoppingCartRepository.saveShoppingCart(shoppingCart);
        return shoppingCart;
    }

    @Override
    public ShoppingCart removeTourFromCart(String tourId, String shoppingCartId) {
        logger.log(Level.INFO, "Removing tour with id {0} from cart with id {1}", new Object[]{tourId, shoppingCartId});
        ShoppingCart shoppingCart = checkIfCartExist(shoppingCartId);
        shoppingCart.removeTour(checkIfTourExist(tourId));
        shoppingCartRepository.saveShoppingCart(shoppingCart);
        return shoppingCart;
    }

    @Override
    public ShoppingCart getBookedToursSummary(String shoppingCartId) {
        logger.log(Level.INFO, "Getting summary of order for cart with id {0}", shoppingCartId);
        ShoppingCart shoppingCart = checkIfCartExist(shoppingCartId);
        PromotionDetails promotionDetails = promotionProvider.calculatePromotion(shoppingCart.getAllAddedTours());
        shoppingCart.swapTours(promotionDetails.getTours());
        Money totalPrice = shoppingCart.getTotalPrice().subtract(promotionDetails.getDiscount());
        logger.log(Level.INFO,
                "Summary for cart with id {0}:\n - Booked tours: {1}\n - Discount: {2}\n - Total price{3}",
                new Object[]{shoppingCartId, promotionDetails.getTours(), promotionDetails.getDiscount(), totalPrice});
        return ShoppingCart.createShoppingCartFrom(shoppingCart, promotionDetails.getTours(), totalPrice);
    }

    @Override
    public boolean removeAllFromCart(String shoppingCartId) {
        logger.log(Level.INFO, "Removing all tours from cart with id {0}", shoppingCartId);
        return shoppingCartRepository.removeShoppingCart(shoppingCartId);
    }

    private ShoppingCart checkIfCartExist(String shoppingCartId) {
        Optional<ShoppingCart> optionalShoppingCart = shoppingCartRepository.getShoppingCart(shoppingCartId);
        return optionalShoppingCart.orElseThrow(() -> {
            logger.log(Level.INFO, "Shopping cart with id {0} not exist ", shoppingCartId);
            throw new ShoppingCartNotFoundException(String.format("Shopping Cart %s not found", shoppingCartId));
        });
    }

    private Tour checkIfTourExist(String tourId) {
        return tourRepository.getTour(tourId).orElseThrow(() -> {
            logger.log(Level.INFO, "Tour with id {0} not exist ", tourId);
            throw new TourNotFoundException(String.format("Tour %s not found", tourId));
        });
    }
}
