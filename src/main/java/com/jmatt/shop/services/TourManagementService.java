package com.jmatt.shop.services;

import com.jmatt.shop.domain.tour.Tour;
import com.jmatt.shop.services.ports.driven.TourManagementRepository;
import com.jmatt.shop.services.ports.driving.TourManagementUseCase;

import java.util.Objects;

public class TourManagementService implements TourManagementUseCase {
    private TourManagementRepository tourManagementRepository;

    public TourManagementService(TourManagementRepository tourManagementRepository) {
        this.tourManagementRepository = Objects.requireNonNull(tourManagementRepository);
    }

    @Override
    public boolean addTourToOffer(Tour tour) {
        return tourManagementRepository.addTour(tour);
    }

    @Override
    public boolean removeTourFromOffer(String tourId) {
        return tourManagementRepository.removeTour(tourId);
    }
}
