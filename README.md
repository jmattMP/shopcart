# Project 
Interview task 

### Business requirements
You have been contracted to build the shopping cart system.

We will start with the following tours in our database

| ID | Name | Price | 
| ---- | ----- | ---- | 
| OH | Opera house tour | $300.00 | 
| BC | Sydney Bridge Climb | $110.00| 
| SK | Sydney Sky Tower | $30.00 | 

As we want to attract attention, we intend to have a few weekly specials:

- We are going to have a 3
for 2 deal on opera house ticket. For example, if
you buy 3 tickets, you will pay the price of 2 only getting another one
completely free of charge.
- We are going to give a free Sky Tower tour for with every Opera House tour
sold
- The Sydney Bridge Climb will have a bulk discount applied, where the price
will drop to $20, if someone buys more than 4

As our Online Sales team is quite indecisive, we want the promotional rules to
be as flexible as possible as they will change in the future.

Our Shopping Cart system can have items added in any order.

Your task is to implement a shopping cart system that meets the requirements
described above.

For example
- Items added to the cart:
OH, OH, OH, BC - Total expected: $710.00
- Items added to the cart: OH, SK - Total expected: $300.00
- Items added to the cart: BC, BC, BC, BC, BC, OH - Total expected: $750

### Technical requirements
- No user interface or command line is required – we are interested in how
you solve the problem only.
-  Please do not use any frameworks unless it is for testing.
-  Please note any assumptions

### Assumptions
- Tours are inexhaustible resource and for booking date of tour is responsible separate module/service.
- Promotion cannot join together, if two promotion could be applied, service automatically choose only based on bigger promotion e.g
value of one order costs $1000 and with promotion A customer pay $850 and with promotion B pay $760, so service choose promotion B.
- Currently service support only AUD currency 

#### Architecture/ design notes
- germ of ports and adapters used as architecture.
- business logic is not complex so only some building blocks from DDD taken in order to avoid complexity and overhead.
- no anti-corruption layer applied - project not to complex and in order to avoid implementation overhead anticoruption layer omitted.
- in real complex problem/solution system anti-corruption layer would be added -> Instead of domain entities, DTOs would be transferred to 
external world (through ports) eg. TourDetails, PromotionDetails, ShoppingCartDetails in order to prevent leaks implementation details and
allowing for changing domain entities without breaking external dependencies. 
- in order to avoid future problems, services are stateless.
- TourBookingUseCase is a public API and all needed information  were added  to provide as much as needed information
in order to implement correct working clients. In this case Runtime exception were added in java doc.
- Admin API not accessible for this scope - no possibility to add new tours from external sources.
  
#### Implementation details
- Collection is returned in public API because is general solution not "backend for frontend" and as general as possible.
- Classes responsible for calculating promotion hold very specific business names in order to emphasize business rules.
